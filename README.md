# Automatic Git Repository Tagging with GitLab CI/CD

This project, "Tagging from CI," aims to simplify the process of tagging your Git repository by automating the tagging process using GitLab CI/CD. With this feature, you can create tags on your repository without having to manually tag them. 

## Prerequisites

- A GitLab account with permissions to create tags on the repository.
- Git installed on your local machine.

## How to Use

1. Clone this repository to your local machine.
2. Update the `.gitlab-ci.yml` file with your repository information and the desired tag name.
3. Push the changes to your GitLab repository.
4. GitLab CI/CD will automatically create the tag on your repository.

## Project Structure

The project has the following structure:

```bash
tagging-from-ci/
├── scripts/
│   └── semantic_version.py
└── .gitlab-ci.yml
```

- `scripts/semantic_version.py`: A Python script that controls the automatic tagging version.
- `.gitlab-ci.yml`: The GitLab CI configuration file.

### .gitlab-ci.yml Description

The `.gitlab-ci.yml` file contains the GitLab CI configuration. It has two stages: `install-python-libraries` and `tagging`.

- The `install-python-libraries` stage installs the required Python libraries and creates a virtual environment. It also runs the `semantic_version.py` script to determine the version number, which is stored in a `build.env` file.

- The `tagging` stage tags the repository with the version number obtained in the previous stage. It also pushes the tag to the remote repository.

### scripts/semantic_version.py Description

The scripts/semantic_version.py script generates the new semantic version based on the latest Git tag. The following steps are performed:

- Get the latest version tag.
- Increment the patch number of the version.
- If no tags are found, create a new version tag with the current year and month.

Please note that the `scripts/semantic_version.py` script is used by the `.gitlab-ci.yml` file to generate the semantic version.

### `git push` Command

The `git push` command in the `tagging` stage pushes the generated Git tag to the Git server with the following format:

```bash
git push --tags http://root:$ACCESS_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git HEAD:main
```
- `root` is the GitLab user with permissions to push to the repository.
- `$ACCESS_TOKEN` is a token with the required permissions to push to the repository. This variable needs to be defined in the GitLab CI/CD settings as a secret variable.
- `$CI_SERVER_HOST` is the hostname of the GitLab server.
- `$CI_PROJECT_PATH` is the path to the GitLab project.

Make sure to replace `$ACCESS_TOKEN` with the actual token before running the GitLab CI pipeline.

## Conclusion

This project demonstrates how to automatically tag a Git repository using GitLab CI and a Python script to generate a new version number. The `semantic_version.py` script can be customized to generate version numbers according to specific requirements. The `.gitlab-ci.yml` file can also be modified to include additional stages and commands as needed.

## Contributing

If you find any issues or have any suggestions, feel free to submit a pull request or open an issue in this repository.

## License

This project is licensed under the MIT License. See the `LICENSE` file for more details.